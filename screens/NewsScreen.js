import React from 'react';
import { ScrollView, StyleSheet, WebView } from 'react-native';
import { WebBrowser } from 'expo';

export default class LinksScreen extends React.Component {
  static navigationOptions = {
    title: 'Neues',
  };

  render() {
    const uri = 'https://bostrot.pro/projects/nippes/';
    return (
        <WebView
          ref={ref => (this.webview = ref)}
          source={{ uri: uri }}
          onError={console.error.bind(console, 'error')}
          bounces={false}
          onShouldStartLoadWithRequest={() => true}
          javaScriptEnabledAndroid={true}
          startInLoadingState={true}
          onNavigationStateChange={(e) => {

            if (e.url !== uri) {
                this.webview.stopLoading();
                Expo.WebBrowser.openBrowserAsync(e.url);
            }
          }}
        />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
