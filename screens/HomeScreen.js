import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';
import { ListItem } from "react-native-elements";
import { WebBrowser } from 'expo';
import HTMLView from 'react-native-htmlview';

import { MonoText } from '../components/StyledText';
var HTMLParser = require('fast-html-parser');

export default class HomeScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      data: [],
      dataTitle: [],
      dataContent: [],
    };
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  static navigationOptions = {
    header: null,
  };

  makeRemoteRequest = () => {
    const url = `http://www.fuer-nippes.de/category/aktuelles/`;
    fetch(url)
      .then(res => res.text())
      .then(res => {
        var root = HTMLParser.parse(res);
        var dataTitle = root.querySelectorAll('.entry-header');
        var dataContent = root.querySelectorAll('.entry-content');
        var dataImage = root.querySelectorAll('.entry-header img');

        var arr = [];
        for (var i in dataTitle) {
          arr[i] = { title: JSON.stringify(dataTitle[i].rawText).replace(/\\n/g, "").replace(/\\t/g, "").replace(/\"/g, ""),
            image: dataImage,
            content: (dataContent[i].rawText).replace("			", "")}
        }
        this.setState({
            data: arr,
          });
        })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
      return(

              <FlatList
                data={this.state.data}
                renderItem={ ({ item }) => (
                  <ListItem
                    button
                    title={(`${item.image}`)}
                    titleNumberOfLines={3}
                    subtitle={
                      <View>
                        <HTMLView
                        value={(`${item.content}`)}
                        style={{ padding: 10 }}
                        />
                      </View>
                    }
                    subtitleNumberOfLines={50}
                  />
              )}
                keyExtractor={item => item.title }
              />
      )
  }

  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
