import React from 'react';
import { Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { TabNavigator, TabBarBottom } from 'react-navigation';

import Colors from '../constants/Colors';

import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import NewsScreen from '../screens/NewsScreen';
import MapScreen from '../screens/MapScreen';
import CameraScreen from '../screens/CameraScreen';

export default TabNavigator(
  {
    News: {
      screen: NewsScreen,
    },
    Maps: {
      screen: MapScreen,
    },
    Camera: {
      screen: CameraScreen,
    },
    Calendar: {
      screen: SettingsScreen,
    },
    Info: {
      screen: SettingsScreen,
    },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
        switch (routeName) {
          case 'News':
            iconName =
              Platform.OS === 'ios'
                ? `ios-paper${focused ? '' : '-outline'}`
                : 'md-paper';
            break;
          case 'Maps':
            iconName = 
              Platform.OS === 'ios' 
              ? `ios-map${focused ? '' : '-outline'}` 
              : 'md-map';
            break;
          case 'Camera':
            iconName =
              Platform.OS === 'ios' 
              ? `ios-camera${focused ? '' : '-outline'}` 
              : 'md-camera';
            break;
          case 'Calendar':
            iconName =
              Platform.OS === 'ios' 
              ? `ios-calendar${focused ? '' : '-outline'}` 
              : 'md-calendar';
            break;
          case 'Info':
            iconName =
              Platform.OS === 'ios' 
              ? `ios-information-circle${focused ? '' : '-outline'}` 
              : 'md-information-circle';
            break;
          }
        return (
          <Ionicons
            name={iconName}
            size={28}
            style={{ marginBottom: -3 }}
            color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
        );
      },
    }),
    tabBarOptions: {
    showLabel: true,
      showIcon: true,
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: true,
    swipeEnabled: true,
  }
);
