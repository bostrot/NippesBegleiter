# Nippes Begleiter

App developed for the citizen club "Für Nippes" with it's headquaters in Cologne.

### Introduction

This app allows you to simply report dusty or unclean spots in Cologne's district Nippes. It gives you a brief overview over the whole district and news over the citizen club. A tutorial on how to use this app will be published soon or on your request.

### Download

You can download our nightly build in the releases page (not available yet). The official version will be uploaded to the Google Play Store.

### Credits

Credits go to all Google library providers and Bostrot.

### Bugs, Errors & Feedback

The app automatically reports new bugs and crashes to our lab. If you still want to share what you did before doing it, please send us an issue under the "Issues" tab. If you have Feedback or any wishes, just tab on the feedback button in the app.
